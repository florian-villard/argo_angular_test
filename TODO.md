## What you have to do

1. We know that we can get the current user folders using an API call. We want to display them in a tree view. Describe quickly how you would organize your code for that.

2. Complete the class `FolderListComponent` to display the folders in a [nested mat-tree](https://v7.material.angular.io/components/tree/overview#nested-tree) view.
    - The component should load folders when initialized
    - The component should convert folders into `FolderNode[]`

3. Now we want to display the total number of folders. Complete the service and the component for this.
    - Implement a new component called `FoldersCount`
    - It should display the total number of folders