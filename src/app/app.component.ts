import { Component } from '@angular/core';
import { User } from './components/UserForm/user-form.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-technical-test';

  user: User = {
    firstName: '',
    lastName: ''
  };
}
