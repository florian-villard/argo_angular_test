import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, catchError, map, Observable, of, throwError } from "rxjs";

export interface Folder {
    id: number;
    name: string;
    parent_id: number | "";
    is_public: boolean;
}

type FoldersResponse = { data: Folder[]; meta: { totalCount: number }};

@Injectable({
	providedIn: 'root',
})
export class FolderService {

    private folders: BehaviorSubject<Folder[]> = new BehaviorSubject<Folder[]>([]);
    public get foldersObservable() {
        return this.folders.asObservable();
    }

    constructor(
		protected http: HttpClient
	) { }


    getFolders(): void {
        const url = 'https://apidev.bear2b.com/v2.13/pic/folders?user_id=1639&is_public=false&_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpZGV2LmJlYXIyYi5jb21cLyIsImlhdCI6MTY3MDkyNjgyNywiZXhwIjoxNjcxNTMxNjI3LCJiYXBpX3R5cGUiOiJub3JtYWwiLCJiYXBpX2FjY2VzcyI6ImZ1bGwiLCJiYXBpX2FwcF9pZCI6OTcsImJhcGlfb3JpZ2luX2FsbG93ZWQiOiIiLCJiYXBpX3VzZXJfaWQiOjE2MzksImJhcGlfY2xpZW50X2lkIjo1NTUsImJhcGlfcm9sZV9pZCI6MCwiYmFwaV9maXJzdF9uYW1lIjoiZ3VpbGxhdW1lMiIsImJhcGlfbGFzdF9uYW1lIjoiUEFOTkVUSUVSIiwiYmFwaV91c2VyX2xhc3RfdXBkYXRlZF90aW1lIjoxNjcwNTgwNjYyLCJzdHFyX3Nlc3Npb24iOiJ4TllrTVgzdmxxb0pqdDVZZUNkVGNRbWk0cUZUTGFnTSIsIm9yaWdpbiI6Imh0dHBzOlwvXC9kZXYtY29ubmV4aW9uLnBpYzM2MC5mciIsInNlc3Npb25faWQiOjE2NzA5MjY4Mjh9.wU8Tn0amjbkV4ETFStWapQTYhOpdfpRGSUs0nK-0MKw';
        of(mockfolderResponse).subscribe((response: FoldersResponse) => {
            this.folders.next(response.data);
        });
    }
}

const mockFolders: Folder[] = [
    { id: 23, name: "root", parent_id: "", is_public: true },
    { id: 24, name: "child1", parent_id: 23, is_public: !1 },
    { id: 25, name: "child11", parent_id: 24, is_public: !0 },
    { id: 26, name: "child12", parent_id: 24, is_public: true },
    { id: 27, name: "child2", parent_id: 23, is_public: false },
    { id: 28, name: "child22", parent_id: 27, is_public: true },
]
const mockfolderResponse: FoldersResponse = {data:mockFolders, meta:{totalCount:6}};