import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { ReactiveFormsModule } from "@angular/forms";

import { UserFormComponent } from "./components/UserForm/user-form.component";
import { FoldersListComponent } from './components/FoldersList/folders-list.component';
import { HttpClientModule } from '@angular/common/http';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { FolderService } from './services/folders.service';

@NgModule({
  declarations: [
    AppComponent,
    UserFormComponent,
    FoldersListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatTreeModule,
    MatIconModule,
  ],
  providers: [FolderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
