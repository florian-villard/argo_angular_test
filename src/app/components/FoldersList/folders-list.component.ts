import { Component, OnInit } from "@angular/core";
import { MatTreeNestedDataSource } from "@angular/material/tree";
import { NestedTreeControl } from "@angular/cdk/tree";
import { Folder, FolderService } from "src/app/services/folders.service";


type FolderNode = Folder & {
    children: FolderNode[];
}

@Component({
    selector: 'app-folders-list',
    styleUrls: ['./folders-list.component.scss'],
    templateUrl: './folders-list.component.html'
})
export class FoldersListComponent implements OnInit {
    dataSource = new MatTreeNestedDataSource<FolderNode>();
    treeControl = new NestedTreeControl<FolderNode>(node => node.children);

    constructor(protected foldersService: FolderService) {}

    ngOnInit() {
        this.foldersService.getFolders();
        this.foldersService.foldersObservable.subscribe(folders => {
            this.dataSource.data = this.foldersToNode(folders);
        });
    }

    foldersToNode(folders: Folder[]): FolderNode[] {
        
        return [];

    }

    hasChild = (_: number, node: FolderNode) => !!node.children && node.children.length > 0;
}