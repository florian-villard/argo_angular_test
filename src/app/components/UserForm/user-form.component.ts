import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

export interface User {
    firstName: string;
    lastName: string;
}

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.scss"]
})
export class UserFormComponent implements OnInit {
  @Input('user') user!: User;
  @Output() userChange: EventEmitter<User> = new EventEmitter<User>();

  userForm!: FormGroup;

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName),
      lastName: new FormControl(this.user.lastName)
    });

    this.userForm.valueChanges.subscribe(() => {
        this.userChange.emit({
            firstName: this.firstName,
            lastName: this.lastName
        });
    });
  }

  get firstName() {
    return this.userForm.value.firstName;
  }

  get lastName() {
    return this.userForm.value.lastName;
  }
}
