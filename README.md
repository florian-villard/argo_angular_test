# AngularTechnicalTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

---
## Folders list feature

In this application, we manage user's projects.
A project has the following structure:

```JS
{
    id: number;
	bookId: number;
	picFolderId: number;
	name: string;
	previewImageUrl: string;
	userId: number;
	isActive: boolean;
	isPrivate: boolean;
	tags: Tag[];
	assignedShorlinks: string[];
	updatedDt: Date;
	createdDt: Date;
}
```

Users can organize their projects into folders.

- The class `FolderService` provide folders of the current user.

- The structure of a Folder is the following:
```TS
{
    id: number;
    name: string;
    parent_id: number | "";
    is_public: boolean;
}
```